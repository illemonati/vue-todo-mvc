module.exports = {
    transpileDependencies: ['vuetify'],
    publicPath: process.env.PUBLIC_URL,
    pwa: {
        name: 'Vue Todo MVC',
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'translucent',
        workboxPluginMode: 'GenerateSW',
    },
};
